import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { GameModule } from "./game/game.module";
import { PublisherModule } from "./publisher/publisher.module";
import { Game } from "./game/game.entity";
import { Publisher } from "./publisher/publisher.entity";

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: "postgres",
      host: "localhost",
      port: 5432,
      database: "testdb",
      entities: [Game, Publisher],
      synchronize: true,
      logging: false
    }),
    GameModule,
    PublisherModule
  ]
})
export class ApplicationModule {}
