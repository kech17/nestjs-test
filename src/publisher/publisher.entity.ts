import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  BaseEntity,
  OneToMany
} from "typeorm";
import { Game } from "../game/game.entity";

@Entity()
export class Publisher extends BaseEntity {
  @PrimaryGeneratedColumn() id: number;

  @Column() name: string;

  @OneToMany(type => Game, game => game.publisher)
  games: Game[];
}
