import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { Publisher } from "./publisher.entity";

@Injectable()
export class PublisherService {
  constructor(
    @InjectRepository(Publisher)
    private readonly publisherRepository: Repository<Publisher>
  ) {}

  async findAll(): Promise<Publisher[]> {
    return await this.publisherRepository.find();
  }

  async create(publisher: Publisher) {
    await this.publisherRepository.save(publisher);
  }
}
