import { Controller, Get, Post, Query } from "@nestjs/common";
import { PublisherService } from "./publisher.service";
import { Publisher } from "./publisher.entity";

@Controller("publisher")
export class PublisherController {
  constructor(private readonly publisherService: PublisherService) {}

  @Get()
  findAll(): Promise<Publisher[]> {
    return this.publisherService.findAll();
  }

  @Post()
  create(@Query("name") name) {
    const publisher = new Publisher();
    publisher.name = name;
    this.publisherService.create(publisher);
  }
}
