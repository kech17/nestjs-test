import { Publisher } from "../publisher/publisher.entity";

export class CreateGameDto {
  readonly title: string;
  readonly description: string;
  readonly publisher: Publisher;
}
