import { Controller, Get, Post, Body } from "@nestjs/common";
import { GameService } from "./game.service";
import { Game } from "./game.entity";
import { CreateGameDto } from "./game.dto";

@Controller("game")
export class GameController {
  constructor(private readonly gameService: GameService) {}

  @Get()
  findAll(): Promise<Game[]> {
    return this.gameService.findAll();
  }

  @Post()
  create(@Body() createGameDto: CreateGameDto) {
    console.log(createGameDto);
    const game = new Game();
    game.title = createGameDto.title;
    game.description = createGameDto.description;
    game.publisher = createGameDto.publisher;
    this.gameService.create(game);
  }
}
