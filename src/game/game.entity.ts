import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  BaseEntity,
  ManyToOne
} from "typeorm";
import { Publisher } from "../publisher/publisher.entity";

@Entity()
export class Game extends BaseEntity {
  @PrimaryGeneratedColumn() id: number;

  @Column() title: string;

  @Column("text") description: string;

  @ManyToOne(type => Publisher, publisher => publisher.games)
  publisher: Publisher;
}
